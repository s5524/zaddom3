﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kurs.BusinessLayer.ModelsDto;
using Kurs.BusinessLayer.Services;
using Kurs.DataLayer.Models;
using Kurs.DataLayer.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace UnitTestProject2
{
    [TestClass]
    public class StudentAndCourseTest
    {
        [TestMethod]
        public void WhenGettingCourseByName_GiveValidCourse_ReturnNotNull()
        {
            //Arrange
            var courseNew = new Course();
            courseNew.CourseName = "Test";// konstruktor
            //courseList.Add(new Course()); // dodajemy ksiazke obojetnie jaka do listy
            //tworzymy mocka
            var courseRepositoryMock = new Mock<ICourseReposietories>();
            courseRepositoryMock.Setup(x => x.GetCourseByName(It.IsAny<string>())).Returns(courseNew); //cookbook zamiast string moze byc

            var courseService = new CourseService(courseRepositoryMock.Object); //interfacami - podajemy mocka w parametrze
            //nazwe ksiazki podamy jako parametr

            //Act

            var result = courseService.GetCourseByName("CookBook");

            //Assert
            //Assert.IsNull(result);
            courseRepositoryMock.Verify(x => x.GetCourseByName("CookBook"), Times.Once); //czy na pewno zapytal ta funkcje z odpowiednim parametrem
            //kazdy test sprawdza jednen przypadek

        }
        [TestMethod]
        public void CheckingCourseByName_GiveValidCourseName_ReturnTrue()
        {
            //Arrange
           
            //courseList.Add(new Course()); // dodajemy ksiazke obojetnie jaka do listy
            //tworzymy mocka
            var courseRepositoryMock = new Mock<ICourseReposietories>();
            courseRepositoryMock.Setup(x => x.CheckCourseByName(It.IsAny<string>())).Returns(true); //cookbook zamiast string moze byc

            var courseService = new CourseService(courseRepositoryMock.Object); //interfacami - podajemy mocka w parametrze
            //nazwe ksiazki podamy jako parametr

            //Act

            var result = courseService.CheckIfCourseNameIsInDB("Test");

            //Assert
            Assert.IsTrue(result);
            courseRepositoryMock.Verify(x => x.CheckCourseByName("Test"), Times.Once); //czy na pewno zapytal ta funkcje z odpowiednim parametrem
            //kazdy test sprawdza jednen przypadek

        }
        [TestMethod]
        public void CheckingCourseByName_GiveinValidCourseName_ReturnFalse()
        {
            //arrange
            var courseRepositoryMock = new Mock<ICourseReposietories>();
            courseRepositoryMock.Setup(x => x.CheckCourseByName(It.IsAny<string>())).Returns(false); //cookbook zamiast string moze byc

            var courseService = new CourseService(courseRepositoryMock.Object); //interfacami - podajemy mocka w parametrze
            
            //Act

            var result = courseService.CheckIfCourseNameIsInDB("Test");

            //Assert
            Assert.IsFalse(result);
            courseRepositoryMock.Verify(x => x.CheckCourseByName("Test"), Times.Once); //czy na pewno zapytal ta funkcje z odpowiednim parametrem
            //kazdy test sprawdza jednen przypadek

        }
        [TestMethod]
        public void CheckingStudentsCountOnCourse_GiveValidStudentsCount_ReturnStudent()
        {
            //arrange
            var studentRepositoryMock = new Mock<IStudentReposietories>();
            var list = new List<int>();
            int temp=3;
            list.Add(temp);
            studentRepositoryMock.Setup(x => x.GetAllStudentsInCourse(It.IsAny<string>())).Returns(list); //cookbook zamiast string moze byc

            var studentService = new StudentService(studentRepositoryMock.Object); //interfacami - podajemy mocka w parametrze

            //Act

            var result = studentService.GetStudentsInCourse("Test");

            //Assert
            Assert.AreEqual(list.Count,result.Count);
            studentRepositoryMock.Verify(x => x.GetAllStudentsInCourse("Test"), Times.Once); //czy na pewno zapytal ta funkcje z odpowiednim parametrem
            //kazdy test sprawdza jednen przypadek

        }
        [TestMethod]
        public void CheckingStudentsNameById_GiveValidStudentsId_ReturnStudentName()
        {
            //arrange
            var studentRepositoryMock = new Mock<IStudentReposietories>();
            string name = "Szymon";


            studentRepositoryMock.Setup(x => x.GetName(It.IsAny<int>())).Returns(name); //cookbook zamiast string moze byc

            var studentService = new StudentService(studentRepositoryMock.Object); //interfacami - podajemy mocka w parametrze

            //Act

            var result = studentService.GetStudentNameById(1);

            //Assert
            Assert.AreEqual(name,result);
            studentRepositoryMock.Verify(x => x.GetName(1), Times.Once); //czy na pewno zapytal ta funkcje z odpowiednim parametrem
            //kazdy test sprawdza jednen przypadek

        }
        [TestMethod]
        public void CheckingStudentsAbsentPoints_GiveValidStudentsIdAndCourseNAme_ReturnStudentPoints()
        {
            //arrange
            var studentRepositoryMock = new Mock<IStudentReposietories>();
            int points = 100;
            int maxPoints = 100;


            studentRepositoryMock.Setup(x => x.PresenceMaxPoints(It.IsAny<int>(),It.IsAny<string>())).Returns(points); //cookbook zamiast string moze byc
            studentRepositoryMock.Setup(x => x.PresencePoints(It.IsAny<int>(), It.IsAny<string>())).Returns(maxPoints); //cookbook zamiast string moze byc

            var studentService = new StudentService(studentRepositoryMock.Object); //interfacami - podajemy mocka w parametrze

            //Act

            var result = studentService.PresenceScore(1,"test");

            //Assert
            Assert.AreEqual(100, result);
            studentRepositoryMock.Verify(x => x.PresencePoints(1,"test"), Times.Once); //czy na pewno zapytal ta funkcje z odpowiednim parametrem
            //kazdy test sprawdza jednen przypadek

        }
        [TestMethod]
        public void CheckingStudentsHomeworkPoints_GiveValidStudentsIdAndCourseNAme_ReturnStudentPoints()
        {
            //arrange
            var studentRepositoryMock = new Mock<IStudentReposietories>();
            int points = 100;
            int maxPoints = 100;


            studentRepositoryMock.Setup(x => x.HomeworkMaxPoints(It.IsAny<int>(), It.IsAny<string>())).Returns(points); //cookbook zamiast string moze byc
            studentRepositoryMock.Setup(x => x.HomeworkPoints(It.IsAny<int>(), It.IsAny<string>())).Returns(maxPoints); //cookbook zamiast string moze byc

            var studentService = new StudentService(studentRepositoryMock.Object); //interfacami - podajemy mocka w parametrze

            //Act

            var result = studentService.HomeworkScore(1, "test");

            //Assert
            Assert.AreEqual(100, result);
            studentRepositoryMock.Verify(x => x.HomeworkPoints(1, "test"), Times.Once); //czy na pewno zapytal ta funkcje z odpowiednim parametrem
            //kazdy test sprawdza jednen przypadek

        }

    }
}
