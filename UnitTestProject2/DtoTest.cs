﻿using System;
using System.Collections.Generic;
using Kurs.BusinessLayer.Mappers;
using Kurs.BusinessLayer.ModelsDto;
using Kurs.DataLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kurs.BusinessLayer.Test
{
    [TestClass]
    public class DtoTest
    {
        [TestMethod]
        public void StudentMapping_ProvideValidStudent_ReceiveProperlyMappedStudentDto()
            {
            //Arrange
            var studentDto = new StudentsDto();
                studentDto.Id = 1;
                studentDto.Name = "Szymon";
                studentDto.Surname = "Zblewski";
                studentDto.Pesel = 1;
                studentDto.Sex = 1;
                studentDto.DateOfBirth=DateTime.Parse("1999-01-01");

            var student = new Student();
                student.Id = 1;
                student.Name = "Szymon";
                student.Surname = "Zblewski";
                student.Pesel = 1;
                student.Sex = 1;
                student.DateOfBirth = DateTime.Parse("1999-01-01");


            //Act
                var students = DtoToEntityModel.StudentDtoToEntityModel(studentDto);

                //Assert
                Assert.AreEqual(students.Id, student.Id);
                Assert.AreEqual(students.Sex, student.Sex);
                Assert.AreEqual(students.Pesel, student.Pesel);
                Assert.AreEqual(students.DateOfBirth, student.DateOfBirth);
                Assert.AreEqual(students.Name, student.Name);
                Assert.AreEqual(students.Surname, student.Surname);

        }
        [TestMethod]
        public void StudentMapping_ProvideValidStudent_ReceiveProperlyMappedStudentEntity()
        {
            //Arrange
            var studentDto = new StudentsDto();
            studentDto.Id = 1;
            studentDto.Name = "Szymon";
            studentDto.Surname = "Zblewski";
            studentDto.Pesel = 1;
            studentDto.Sex = 1;
            studentDto.DateOfBirth = DateTime.Parse("1999-01-01");

            var student = new Student();
            student.Id = 1;
            student.Name = "Szymon";
            student.Surname = "Zblewski";
            student.Pesel = 1;
            student.Sex = 1;
            student.DateOfBirth = DateTime.Parse("1999-01-01");


            //Act
            var students =EntityModelToDto.StudentEntityModelToDto(student);

            //Assert
            Assert.AreEqual(students.Id, studentDto.Id);
            Assert.AreEqual(students.Sex, studentDto.Sex);
            Assert.AreEqual(students.Pesel, studentDto.Pesel);
            Assert.AreEqual(students.DateOfBirth, studentDto.DateOfBirth);
            Assert.AreEqual(students.Name, studentDto.Name);
            Assert.AreEqual(students.Surname, studentDto.Surname);

        }

        [TestMethod]
        public void CourseMapping_ProvideValidCourse_RecivePropellyMappesCourseDto()
        {
            var studentDto = new StudentsDto();
            studentDto.Id = 1;
            studentDto.Name = "Szymon";
            studentDto.Surname = "Zblewski";
            studentDto.Pesel = 1;
            studentDto.Sex = 1;
            studentDto.DateOfBirth = DateTime.Parse("1999-01-01");


            var courseDto = new CourseDto();
            courseDto.Id = 1;
            courseDto.CourseDate = DateTime.Parse("1999-11-11");
            courseDto.CourseName = "c#";
            courseDto.CourseOwner = "Adam";
            courseDto.PercentAbsence = 100;
            courseDto.PercentHomework = 100;
            courseDto.StudentsDto = new List<StudentsDto>();
            courseDto.StudentsDto.Add(studentDto);

            var student = new Student();
            student.Id = 1;
            student.Name = "Szymon";
            student.Surname = "Zblewski";
            student.Pesel = 1;
            student.Sex = 1;
            student.DateOfBirth = DateTime.Parse("1999-01-01");

            var course = new Course();
            course.Id = 1;
            course.CourseDate = DateTime.Parse("1999-11-11");
            course.CourseName = "c#";
            course.CourseOwner = "Adam";
            course.PercentAbsence = 100;
            course.PercentHomework = 100;
            course.Students=new List<Student>();
            course.Students.Add(student);

            var courseDtos = DtoToEntityModel.CourseToDtoEntityModel(courseDto);

            Assert.AreEqual(courseDtos.Id, course.Id);
            Assert.AreEqual(courseDtos.CourseDate, course.CourseDate);
            Assert.AreEqual(courseDtos.CourseName, course.CourseName);
            Assert.AreEqual(courseDtos.CourseOwner, course.CourseOwner);
            Assert.AreEqual(courseDtos.PercentAbsence, course.PercentAbsence);
            Assert.AreEqual(courseDtos.PercentHomework, course.PercentHomework);
            //Assert.AreEqual(courseDtos.Students, course.Students);
            Assert.IsTrue(courseDtos.Students[0].Equals(course.Students[0]));
            


        }
        [TestMethod]
        public void CourseMapping_ProvideValidCourse_RecivePropellyMappesCourseEntity()
        {
            var studentDto = new StudentsDto();
            studentDto.Id = 1;
            studentDto.Name = "Szymon";
            studentDto.Surname = "Zblewski";
            studentDto.Pesel = 1;
            studentDto.Sex = 1;
            studentDto.DateOfBirth = DateTime.Parse("1999-01-01");


            var courseDto = new CourseDto();
            courseDto.Id = 1;
            courseDto.CourseDate = DateTime.Parse("1999-11-11");
            courseDto.CourseName = "c#";
            courseDto.CourseOwner = "Adam";
            courseDto.PercentAbsence = 100;
            courseDto.PercentHomework = 100;
            courseDto.StudentsDto = new List<StudentsDto>();
            courseDto.StudentsDto.Add(studentDto);

            var student = new Student();
            student.Id = 1;
            student.Name = "Szymon";
            student.Surname = "Zblewski";
            student.Pesel = 1;
            student.Sex = 1;
            student.DateOfBirth = DateTime.Parse("1999-01-01");

            var course = new Course();
            course.Id = 1;
            course.CourseDate = DateTime.Parse("1999-11-11");
            course.CourseName = "c#";
            course.CourseOwner = "Adam";
            course.PercentAbsence = 100;
            course.PercentHomework = 100;
            course.Students = new List<Student>();
            course.Students.Add(student);

            var courses = EntityModelToDto.CourseEntityModelToDto(course);

            Assert.AreEqual(courseDto.Id, courses.Id);
            Assert.AreEqual(courseDto.CourseDate, courses.CourseDate);
            Assert.AreEqual(courseDto.CourseName, courses.CourseName);
            Assert.AreEqual(courseDto.CourseOwner, courses.CourseOwner);
            Assert.AreEqual(courseDto.PercentAbsence, courses.PercentAbsence);
            Assert.AreEqual(courseDto.PercentHomework, courses.PercentHomework);
            //Assert.AreEqual(courseDtos.Students, course.Students);
            //Assert.IsTrue(student.Equals(studentDto));
            Assert.IsTrue(courses.StudentsDto[0].Equals(courseDto.StudentsDto[0]));



        }
        [TestMethod]
        public void StudentMapping_ProvideInValidStudent_ReceiveProperlyMappedStudentToEntity()
        {
            //Arrange
            var student = new StudentsDto();
            student = null;


            //Act
            var students = DtoToEntityModel.StudentDtoToEntityModel(student);

            //Assert
            Assert.IsNull(students);
            
        }
        [TestMethod]
        public void StudentMapping_ProvideInValidStudent_ReceiveProperlyMappedStudentDto()
        {
            //Arrange
            var student = new Student();
            student = null;


            //Act
            var students = EntityModelToDto.StudentEntityModelToDto(student);

            //Assert
            Assert.IsNull(students);

        }
        [TestMethod]
        public void CourseMapping_ProvideInValidCourse_ReceiveProperlyMappedCourseToEntity()
        {
            //Arrange
            var courseDto = new CourseDto();;
            courseDto = null;


            //Act
            var courseDtos = DtoToEntityModel.CourseToDtoEntityModel(courseDto);

            //Assert
            Assert.IsNull(courseDtos);

        }
        [TestMethod]
        public void CourseMapping_ProvideInValidStudent_ReceiveProperlyMappedCourseDto()
        {
            //Arrange
            var course = new Course();
            course = null;


            //Act
            var courses = EntityModelToDto.CourseEntityModelToDto(course);

            //Assert
            Assert.IsNull(courses);

        }
    }
}
