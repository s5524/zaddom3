﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kurs.BusinessLayer.Services;
using Newtonsoft.Json;

namespace JsonApp
{
    class JsonStarter
    {
        public void Loop()
        {
            Console.WriteLine("Podaj sciezke do pliku");
            var name = Console.ReadLine();
            Console.WriteLine("Podaj nazwe pliku");
            var name1 = Console.ReadLine();
            try
            {
                using (StreamReader r = new StreamReader(name +"\\"+ name1 + ".json"))
                {
                    string json = r.ReadToEnd();
                    var items = JsonConvert.DeserializeObject<RaportServive>(json);
                    Console.WriteLine("Prowadzący kurs: "+items.courseOwner);
                    Console.WriteLine("Ile procent osob zaliczyło po obecnosci: "+items.howMuchPasedPresence);                    
                    Console.WriteLine("Ile procent osob zaliczylo po zadaniu domowym: "+items.howMuchPassedHomework);                    
                    Console.WriteLine("Procent obecnosci: "+items.percentAbsence);                    
                    Console.WriteLine("Procent zadan domowych: "+items.percentHomework);
                    Console.WriteLine("Nazwa raportu: "+items.raportName);


                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Złaściezka");
                Loop();
            
            }
            

        }

        public class Item
            {
                public string raportName;
                public string courseOwner;
                public int percentHomework;
                public int percentAbsence;
                public int howMuchPassedHomework;
                public int howMuchPasedPresence;

        }
    }
    }

