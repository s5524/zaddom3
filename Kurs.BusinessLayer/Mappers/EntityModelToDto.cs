﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kurs.BusinessLayer.ModelsDto;
using Kurs.DataLayer.Models;

namespace Kurs.BusinessLayer.Mappers
{
    public class EntityModelToDto
    {
        public static StudentsDto StudentEntityModelToDto(Student students)
        {
            if (students == null)
            {
                return null;
            }

            var studentsDto = new StudentsDto();
            studentsDto.Id = students.Id;
            studentsDto.DateOfBirth = students.DateOfBirth;
            studentsDto.Name = students.Name;
            studentsDto.Surname = students.Surname;
            studentsDto.Pesel = students.Pesel;
            studentsDto.Sex = students.Sex;
            //studentsDto.Courses = students.Courses.Select(x => CourseEntityModelToDto(x)).ToList();

            return studentsDto;
        }

        public static CourseDto CourseEntityModelToDto(Course course)
        {
            if (course == null)
            {
                return null;
            }

            var courseDto = new CourseDto();
            courseDto.CourseDate = course.CourseDate;
            courseDto.CourseName = course.CourseName;
            courseDto.CourseOwner = course.CourseOwner;
            courseDto.Id = course.Id;
            courseDto.PercentAbsence = course.PercentAbsence;
            courseDto.PercentHomework = course.PercentHomework;
            courseDto.StudentsDto = StudentDtoToStudentEntity(course.Students); ;
            
            return courseDto;
        }
        public static List<StudentsDto> StudentDtoToStudentEntity(List<Student> student)
        {
            List<StudentsDto> list = new List<StudentsDto>();
            if (student == null)
            {
                return list;
            }
            foreach (var students in student)
            {
                list.Add(StudentEntityModelToDto(students));
            }
            return list;
        }
    }
}
