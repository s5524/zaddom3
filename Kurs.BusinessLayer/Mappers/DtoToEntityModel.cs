﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kurs.BusinessLayer.ModelsDto;
using Kurs.DataLayer.Models;

namespace Kurs.BusinessLayer.Mappers
{
    public class DtoToEntityModel
    {
        public static Student StudentDtoToEntityModel(StudentsDto studentsDto)
        {
            if (studentsDto == null)
            {
                return null;
            }

            var student = new Student();
            student.Id = studentsDto.Id;
            student.DateOfBirth = studentsDto.DateOfBirth;
            student.Name = studentsDto.Name;
            student.Surname = studentsDto.Surname;
            student.Pesel = studentsDto.Pesel;
            student.Sex = studentsDto.Sex;
                
            return student;
        }

        public static Course CourseToDtoEntityModel(CourseDto courseDto)
        {
            if (courseDto == null)
            {
                return null;
            }

            var course = new Course();
            course.CourseDate = courseDto.CourseDate;
            course.CourseName = courseDto.CourseName;
            course.CourseOwner = courseDto.CourseOwner;
            course.Id = courseDto.Id;
            course.PercentAbsence = courseDto.PercentAbsence;
            course.PercentHomework = courseDto.PercentHomework;
            course.Students = StudentEntityToStudentDto(courseDto.StudentsDto);
            return course;
        }
        public static List<Student> StudentEntityToStudentDto(List<StudentsDto> studentDto)
        {
            List<Student> list = new List<Student>();
            if (studentDto == null)
            {
                return list;
            }
            foreach (var student in studentDto)
            {
                list.Add(StudentDtoToEntityModel(student));
            }
            return list;
        }
    }
}
