﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kurs.DataLayer.Models;
using Newtonsoft.Json;

namespace Kurs.BusinessLayer.Services
{
    public class RaportServive : IRaportServive
    {
        //name presentscore,homework score,
        
        //public List<StudentScore> raportService;
        public string raportName;
        public string courseOwner;
        public int percentHomework;
        public int percentAbsence;
        public int howMuchPassedHomework;
        public int howMuchPasedPresence;

    }

    //public class StudentScore
    //{
    //    public string Name { get; set; }
    //    public int HomeworkScore { get; set; }
    //    public int Score { get; set; }
    //}
}
