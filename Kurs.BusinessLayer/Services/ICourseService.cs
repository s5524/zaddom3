using System;
using Kurs.BusinessLayer.ModelsDto;

namespace Kurs.BusinessLayer.Services
{
    public interface ICourseService
    {
        CourseDto GetCourseByName(string name);
        void AddCourse(CourseDto courseDto);
        bool CheckIfCourseNameIsInDB(string name);

        void AddHomeworks(string name,int score, int maxScore, DateTime day,int studentId)//Presence presence, int courseId, int studentId
            ;

        void AddPresenDay(int id, int presence, DateTime date, string name);
        void UpdateCourse(CourseDto courseRep);
        int UpdatePercent(int courseRepPercentAbsence, string tempInt);
    }
}