﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStores.DataLayer.Repositories;
using Kurs.BusinessLayer.Mappers;
using Kurs.BusinessLayer.ModelsDto;
using Kurs.DataLayer;
using Kurs.DataLayer.Models;
using Kurs.DataLayer.Repositories;

namespace Kurs.BusinessLayer.Services
{
    public class StudentService : IStudentService
    {
        //private GenericRepository<Course> _genericRepositoryCourse;
        private GenericRepository<Student> _genericRepositoryStudent;
        private CourseRepositoryServices _geCourseRepositoryServices;

        
        public StudentService()
        {
          //  _genericRepositoryCourse = CourseUnitOfWork.GetInstance().GetRepository<Course>();
            _genericRepositoryStudent = CourseUnitOfWork.GetInstance().GetRepository<Student>();
        //    _genericRepositoryH = CourseUnitOfWork.GetInstance().GetRepository<Homework>();
         //   _genericRepositoryP = CourseUnitOfWork.GetInstance().GetRepository<Presence>();

            _geCourseRepositoryServices = new CourseRepositoryServices();
        }

        

        public bool CheckIfStudentExistsInDBByPesel(int pesel)
        {
            var studentPesel = _genericRepositoryStudent.GetAll().Count(x => x.Pesel == pesel);
            
            return studentPesel==0;
        }

        public bool CheckHowMuchStudentsIs(int check)
        {
            var studentCount = _geCourseRepositoryServices.checkStudentsCount(check);
            return studentCount;
        }

        public void addStudent(StudentsDto studentsDto)
        {
            var students = DtoToEntityModel.StudentDtoToEntityModel(studentsDto);
            _genericRepositoryStudent.Attath(students);
            _genericRepositoryStudent.Add(students);
            CourseUnitOfWork.GetInstance().SaveChanges();
        }

        public void UpdateStudent(StudentsDto student)
        {
            var students = DtoToEntityModel.StudentDtoToEntityModel(student);
            _genericRepositoryStudent.Update(students);
            CourseUnitOfWork.GetInstance().SaveChanges();

        }

        public StudentsDto TakeStudentByPesel(int pesel)
        {
            var studentByPesel = _genericRepositoryStudent.GetAll().FirstOrDefault(x=>x.Pesel==pesel);//.TakeCarById(id);
            return EntityModelToDto.StudentEntityModelToDto(studentByPesel);
        }

        public void RemoveStudent(StudentsDto student,CourseDto course)
        {
            var studentToRemove = DtoToEntityModel.StudentDtoToEntityModel(student);
            var courses = DtoToEntityModel.CourseToDtoEntityModel(course);
            _geCourseRepositoryServices.RemoveStudent(studentToRemove, courses);
            CourseUnitOfWork.GetInstance().SaveChanges();

        }

        public void AddStudentByPeselToCourses(int pesel,string courseName)
        {
            _geCourseRepositoryServices.AddStudentByPeselToCourse(pesel,courseName);
            CourseUnitOfWork.GetInstance().SaveChanges();

        }

        public List<int> GetStudentsInCourse(string courseName)
        {
            List<int> list = _geCourseRepositoryServices.GetAllStudentsInCourse(courseName);
            ;
            return list;
        }


        public string GetStudentNameById(int id)
        {
            var students = _genericRepositoryStudent.GetAll().First(x => x.Id == id);
            var course = students.Name;
            return course;

           
        }
        public void AddPresenDay(int id, int presence, DateTime date,string name)
        {
            Presence presences = new Presence();
            presences.PresencDate = date;
            presences.IsPresence = presence;
            _geCourseRepositoryServices.AddPresenceDay(presences,name,id);
            CourseUnitOfWork.GetInstance().SaveChanges();

        }

        public int HomeworkScore(int studentId, string courseName)
        {
            var course = new CourseRepositoryServices();
            var studentHomeworkScore = 0;
            var maxHomeworkScore = 0;
            var studentPresence = course.GetHomework().Where(s => s.Student.Id == studentId);
            var cos = studentPresence.Where(y => y.Course.CourseName == courseName);
            foreach (var log in cos)
            {
                studentHomeworkScore = log.StudentScore + studentHomeworkScore;
                maxHomeworkScore = log.MaxScore + maxHomeworkScore;
            }
            //StudentReposietories percentPresence = new StudentReposietories();
            double score = ((double)studentHomeworkScore / (double)maxHomeworkScore) * 100;
            //if (score < 0) score = 0;
            return (int)score;
        }

        //public int PresenceScore(int i, string courseName)
        //{

        //}
        public int PresenceScore(int studentId, string courseName)
        {
            var course = new CourseRepositoryServices();
            var studentMaxPresence = 0;
            var maxAbsence = 0;
            var studentPresence = course.GetPresence().Where(s => s.Student.Id == studentId);
            var cos = studentPresence.Where(y => y.Course.CourseName == courseName);
            foreach (var log in cos)
            {
                studentMaxPresence = log.IsPresence + studentMaxPresence;
                maxAbsence++;
            }
            //StudentReposietories percentPresence = new StudentReposietories();
            double score = ((double)studentMaxPresence / (double)maxAbsence) * 100;
            //if (score < 0) score = 0;
            return (int)score;
        }
        public string IsPased(int studentId, string courseName)
        {
            var course = new CourseRepositoryServices();
            int studentMaxPresence = 0;
            var maxAbsence = 0;
            var studentPresence = course.GetPresence().Where(s => s.Student.Id == studentId);
            var cos = studentPresence.Where(y => y.Course.CourseName == courseName);
            var studentHomework = course.GetHomework().Where(s => s.Student.Id == studentId);
            var cos1 = studentHomework.Where(y => y.Course.CourseName == courseName);
            var studentMaxHomeworkPoints = 0;
            var studentHomeworkPoints = 0;

            foreach (var log in cos)
            {
                studentMaxPresence = log.IsPresence + studentMaxPresence;
                maxAbsence++;
            }
            foreach (var log in cos1)
            {
                studentMaxHomeworkPoints = log.MaxScore + studentMaxHomeworkPoints;
                studentHomeworkPoints = log.StudentScore + studentHomeworkPoints;
            }
            double score = (double)studentMaxPresence / (double)maxAbsence * 100;
            double scoreH = ((double)studentHomeworkPoints / (double)studentMaxHomeworkPoints) * 100;
            var percentPresences = course.GetCoursesWitchHomework().First(x => x.CourseName == courseName)
                .PercentAbsence;//_genericRepositoryCourse.GetAll().FirstOrDefault(x=>x.CourseName==courseName).PercentAbsence;
            var peercentHomework = course.GetCoursesWitchHomework().FirstOrDefault(x => x.CourseName == courseName).PercentHomework;
            if (peercentHomework < 0)
            {
                peercentHomework = 0;
            }
            string pass;
            if (score >= percentPresences && scoreH >= peercentHomework)
            {

                pass = "ZALICZONE";
            }
            else
            {
                pass = "NIEZALICZONE";
            }
            return pass;
        }

        public DateTime UpdatePercent(DateTime studentsDateOfBirth, string tempDate)
        {
            DateTime tempDateTime=DateTime.Parse("11.11.11");
            bool t = true;
            while (t)
            {
                if (string.IsNullOrEmpty(tempDate))
                {
                    tempDateTime = studentsDateOfBirth;
                    t = false;
                }
                else if (DateTime.TryParse(tempDate, out tempDateTime) == true)
                {
                        t = false;
                }
                else
                {
                    Console.WriteLine("Podaj poprawną datę");
                    tempDate = Console.ReadLine();
                }
            }
            return tempDateTime;
        }
    }

}
