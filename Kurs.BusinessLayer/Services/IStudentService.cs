using System;
using System.Collections.Generic;
using Kurs.BusinessLayer.ModelsDto;

namespace Kurs.BusinessLayer.Services
{
    public interface IStudentService
    {
        string IsPased(int i, string courseName);
        bool CheckIfStudentExistsInDBByPesel(int pesel);
        bool CheckHowMuchStudentsIs(int check);
        void addStudent(StudentsDto studentsDto);
        void UpdateStudent(StudentsDto student);
        StudentsDto TakeStudentByPesel(int pesel);
        void RemoveStudent(StudentsDto student,CourseDto course);
        void AddStudentByPeselToCourses(int pesel,string courseName);
        List<int> GetStudentsInCourse(string courseName);
        string GetStudentNameById(int id);
        void AddPresenDay(int id, int presence, DateTime date,string name);
        int HomeworkScore(int i, string courseName);
        int PresenceScore(int i, string courseName);
        DateTime UpdatePercent(DateTime studentsDateOfBirth, string tempDate);
    }
}