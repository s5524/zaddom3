﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStores.DataLayer.Repositories;
using Kurs.BusinessLayer.Mappers;
using Kurs.BusinessLayer.ModelsDto;
using Kurs.DataLayer;
using Kurs.DataLayer.Models;
using Kurs.DataLayer.Repositories;

namespace Kurs.BusinessLayer.Services
{
    public class CourseService //: ICourseService
        : ICourseService
    {
        private GenericRepository<Course> _genericRepositoryCourse;
        private GenericRepository<Student> _genericRepositoryStudent;
        private GenericRepository<Presence> _genericRepositoryP;
        private GenericRepository<Homework> _genericRepositoryH;

       
        public CourseService()
        {
            _genericRepositoryCourse = CourseUnitOfWork.GetInstance().GetRepository<Course>();
            _genericRepositoryStudent = CourseUnitOfWork.GetInstance().GetRepository<Student>();
            _genericRepositoryH = CourseUnitOfWork.GetInstance().GetRepository<Homework>();
            _genericRepositoryP = CourseUnitOfWork.GetInstance().GetRepository<Presence>();

        }
       
        public CourseDto GetCourseByName(string name)
        {
            var courseCheckByName = _genericRepositoryCourse.GetAll().First(x => x.CourseName == name);
            return EntityModelToDto.CourseEntityModelToDto(courseCheckByName);

        }

        public void AddCourse(CourseDto courseDto)
        {

            var course = DtoToEntityModel.CourseToDtoEntityModel(courseDto);
            _genericRepositoryCourse.Attath(course);
            _genericRepositoryCourse.Add(course);
            CourseUnitOfWork.GetInstance().SaveChanges();

        }

        public bool CheckIfCourseNameIsInDB(string name)
        {
            var courseGenericRepName = _genericRepositoryCourse.GetAll().Count(x => x.CourseName == name);
            return courseGenericRepName == 0;
        }

        
        public void AddHomeworks(string name,int score, int maxScore, DateTime day,int studentId)//Presence presence, int courseId, int studentId
        {
            var homework = new Homework();
            homework.MaxScore = maxScore;
            homework.StudentScore = score;
            homework.HomeworkDate = day;
            homework.Course = _genericRepositoryCourse.GetAll().First(x => x.CourseName == name);
            homework.Student = _genericRepositoryStudent.GetAll().First(x => x.Id == studentId);
            _genericRepositoryH.Attath(homework);
            _genericRepositoryH.Add(homework);
            CourseUnitOfWork.GetInstance().SaveChanges();


        }
        public void AddPresenDay(int id, int presence, DateTime date, string name)
        {
            var presences = new Presence();
            presences.Course = _genericRepositoryCourse.GetAll().First(x => x.CourseName == name);
            presences.IsPresence = presence;
            presences.PresencDate = date;
            presences.Student = _genericRepositoryStudent.GetAll().First(x => x.Id == id);
            _genericRepositoryP.Attath(presences);
            _genericRepositoryP.Add(presences);
            CourseUnitOfWork.GetInstance().SaveChanges();

        }
        public void UpdateCourse(CourseDto courseRep)
        {
            var course = DtoToEntityModel.CourseToDtoEntityModel(courseRep);
            var coursesInDb = _genericRepositoryCourse.GetAll().Include(x=>x.Homeworks).Include(y=>y.Students).Include(z=>z.Presence).FirstOrDefault(x=>x.Id==course.Id); //PresenceDbSet.Where(s => s.Student.Id == studentId);
            if (!string.IsNullOrEmpty(course.CourseName))
            {
                coursesInDb.CourseName = course.CourseName;

            }
            
            if (!string.IsNullOrEmpty(course.CourseOwner))
            {
                coursesInDb.CourseOwner = course.CourseOwner;
            }
            coursesInDb.PercentAbsence = course.PercentAbsence;
            coursesInDb.PercentHomework = course.PercentHomework;
            _genericRepositoryCourse.Update(coursesInDb);
            CourseUnitOfWork.GetInstance().SaveChanges();

        }

        public int UpdatePercent(int courseRepPercentAbsence, string tempInt)
        {
            int tempPercent= 0;
            bool t = true;
            while (t)
            {
                if (string.IsNullOrEmpty(tempInt))
                {
                    tempPercent =  courseRepPercentAbsence;
                    t = false;
                }
                else if (int.TryParse(tempInt, out tempPercent) == true)
                {
                    if (tempPercent >= 0 && tempPercent <= 100)
                    {
                        t = false;
                    }
                    else
                    {
                        tempInt = "a";
                    }
                }
                else
                {
                    Console.WriteLine("Podaj procent 0-100");
                    tempInt = Console.ReadLine();
                }
            }
            return tempPercent;
        }
    }
}
