﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kurs.DataLayer.Models;

namespace Kurs.BusinessLayer.ModelsDto
{
    public class CourseDto
    {
        public int Id;
        public string CourseName;
        public string CourseOwner;
        public DateTime CourseDate;
        public int PercentHomework;
        public int PercentAbsence; 
        public List<StudentsDto> StudentsDto; 
        public List<Presence> Presence; 
        public List<Homework> Homeworks; 

    }
}
