﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kurs.DataLayer.Models;

namespace Kurs.BusinessLayer.ModelsDto
{
    public class StudentsDto
    {
        
        public int Id;
        public int Pesel;
        public string Name;
        public string Surname;
        public DateTime DateOfBirth;
        public int Sex;
        public List<CourseDto> Courses;
        public List<Presence> Presences;
        public List<Homework> Homeworks;


        public override bool Equals(object obj)
        {
            if (obj == null || obj as StudentsDto == null)//strawdza w drzewie dziedziczenia czy moze zrobic ksiazke'sprawdzanie typu'
            {
                return false;
            }
            var objAsStudentDto = obj as StudentsDto;
            var areEqyal = true;
            areEqyal = areEqyal && objAsStudentDto.Id == Id;
            areEqyal = areEqyal && objAsStudentDto.Pesel == Pesel;
            areEqyal = areEqyal && objAsStudentDto.Name == Name;
            areEqyal = areEqyal && objAsStudentDto.Surname == Surname;
            areEqyal = areEqyal && objAsStudentDto.DateOfBirth == DateOfBirth;
            areEqyal = areEqyal && objAsStudentDto.Sex == Sex;
            return areEqyal;
        }
    }

}
