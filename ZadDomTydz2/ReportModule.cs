﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kurs.BusinessLayer.Services;
using Ninject.Modules;
using Kurs.DataLayer.Repositories;


namespace ZadDomTydz2
{
    class ReportModule : NinjectModule
    {
        
            public override void Load()
            {
                Bind<IStudentService>().To<StudentService>();
                Bind<ICourseService>().To<CourseService>();
            }
        }
    }

