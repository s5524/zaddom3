﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Kurs.BusinessLayer.ModelsDto;
using Kurs.BusinessLayer.Services;
using Newtonsoft.Json;
using Ninject;


namespace ZadDomTydz2
{

    internal class Program
    {
        public delegate void SavedEventHandler(object sender, SavingEventArgs args);
        public event SavedEventHandler Save;
        
        private ICourseService _courseService;
        private IStudentService _studentService;
        private CommandDispatcher _commandDispatcher;
        private IRaportServive _raportServive;
        bool exit = false;

        public Program(ICourseService courseService,IStudentService studentService)
        {
            _courseService = courseService;
            _studentService = studentService;
        }

        static void Main(string[] args)
        {

            var start = ConfigureContainer();
            start.Get<Program>().Start();
        }
        public void Start()
        {
            DefineCommands();
            while (!exit)
            {
                var command = GetInt("Podaj opcję:\n1 Dodaj studentów\n2 Dodaj kurs\n3 Dodaj dzień obecności\n4 Dodaj zadanie domowe\n5 Wypisz raport\n6 Zmień dane studenta\n" +
                                     "7 Zmien dane kursu\n8 Wyjscie\n");

                if (!_commandDispatcher.CheckIfCommandExists(command))
                {
                    Console.WriteLine("Command undefined. Try again...");
                }
                else
                {
                    _commandDispatcher.ExecuteCommand(command);
                }
            }
        }
        
        private void DefineCommands()
        {
            _commandDispatcher = new CommandDispatcher();
            _commandDispatcher.AddCommand(1, CreateStudentList);
            _commandDispatcher.AddCommand(2, CreateCourse);
            _commandDispatcher.AddCommand(3, AddPresenceDay);
            _commandDispatcher.AddCommand(4, AddHomework);
            _commandDispatcher.AddCommand(5, MakeRaport);
            _commandDispatcher.AddCommand(6, UpdateStudent);
            _commandDispatcher.AddCommand(7, UpdateCourse);
            _commandDispatcher.AddCommand(8, Exit);


        }

        public void Exit()
        {
            exit = true;
        }

        public void UpdateCourse()
        {
            var courseName = CheckCoursNameInDBisExist(GetString("Podaj nazwe kursu do zmiany"));
            

            var courseRep = _courseService.GetCourseByName(courseName);
            var chose = GetSex("1 zmien dane kursu 2 usun studeta z kursu\n");
            if (chose == 1)
            {
                courseRep.CourseName = GetString("Podaj nowa nazw kursu\n");
                courseRep.CourseOwner = GetString("Podaj nowe imię prowadzącego\n");
                Console.WriteLine("Podaj procent obecnoscie do zaliczenia");
                var tempAbsence = Console.ReadLine();
                courseRep.PercentAbsence = _courseService.UpdatePercent(courseRep.PercentAbsence, tempAbsence);
                Console.WriteLine("Podaj procent zadan domowych do zaliczenia");
                var tempHomework = Console.ReadLine();
                courseRep.PercentHomework = _courseService.UpdatePercent(courseRep.PercentHomework, tempHomework);
                _courseService.UpdateCourse(courseRep);

            }
            else
            {
                //var studentServices = new StudentService();
                var studentPesel = _studentService.TakeStudentByPesel(CheckPeselIsInDB("Podaj pesel studenta którego dane chcesz zmianić"));
                _studentService.RemoveStudent(studentPesel,courseRep);
            }
        }

        private string CheckCourseNameByRegex(string CourseName)
        {
            Regex regex = new Regex(@"^C#_\w+_[A-Z]{2}$");
            bool t = true;
            string courseName = "";
            while (t)
            {
                courseName = CheckCoursNameInDB(CourseName);

                if (regex.IsMatch(courseName))
                {
                    t=false;
                }
                else
                {
                    Console.WriteLine("Podaj poprawna nazwe kursu ze wzorca");
                    CourseName = Console.ReadLine();
                }
                
            }
            return courseName;
        }

        public void UpdateStudent()
        {
            var studentPesel = CheckPeselIsInDB("Podaj pesel studenta którego dane chcesz zmianić");

            var students = _studentService.TakeStudentByPesel(studentPesel);
            var student = new StudentsDto();
            var course = new CourseDto();
            var chose = GetSex("1 usuń studenta z kursu 2 zmień dane studenta\n");
            if (chose == 2)
            {
                students.Name = GetString("Podaj imię\n");
                students.Surname = GetString("Podaj nazwisko\n");
                Console.WriteLine("Podaj date urodzenia");
                var tempDate = Console.ReadLine();
                students.DateOfBirth = _studentService.UpdatePercent(students.DateOfBirth, tempDate);


                _studentService.UpdateStudent(students);
            }
            else
            {
                var courseName = CheckCoursNameInDBisExist(GetString("Podaj nazwę kursu\n"));
                course = _courseService.GetCourseByName(courseName);
                _studentService.RemoveStudent(students,course);
            }

        }

        public void MakeRaport()
        {
            Save += Saved;
            var courseName = CheckCoursNameInDBisExist(GetString("Podaj nazwę kursu\n"));
            var list = _studentService.GetStudentsInCourse(courseName);
            for (int i = 0; i < list.Count; i++)
            {
                var student = _studentService.GetStudentNameById(list[i]);
                
                Console.Write("Student "+student+" otrzymał "+_studentService.PresenceScore(list[i],courseName) +"% za obecność");
                Console.WriteLine(" oraz "+ _studentService.HomeworkScore(list[i], courseName) +"% za zadania domowe " +_studentService.IsPased(list[i],courseName));
            } 

            OnSaved(DataFromRaport(courseName));

        }

        public RaportServive DataFromRaport(string courseName)
        {
            var list = _studentService.GetStudentsInCourse(courseName);
            var raportService = new RaportServive();
            var course = _courseService.GetCourseByName(courseName);
            raportService.raportName = course.CourseName;
            raportService.courseOwner = course.CourseOwner;
            raportService.howMuchPassedHomework=0;
            raportService.percentAbsence = 0;
            raportService.percentHomework = 0;
            for (int i = 0; i < list.Count; i++)
            {
                var student = _studentService.GetStudentNameById(list[i]);

                int tempPresenceScore = _studentService.PresenceScore(list[i], courseName);
                raportService.percentAbsence = tempPresenceScore+raportService.percentAbsence;
                int tempHomeworkScore = _studentService.HomeworkScore(list[i], courseName);
                raportService.percentHomework = tempHomeworkScore + raportService.percentHomework;
                if (_studentService.IsPased(list[i], courseName) == "ZALICZONE")
                {
                    raportService.howMuchPassedHomework++;
                };
            }

            raportService.percentAbsence = raportService.percentAbsence / list.Count;
            raportService.percentHomework = raportService.percentHomework / list.Count;

            return raportService;
        }
        public void Saved(object sender, SavingEventArgs args)
        {
            //StringBuilder sb = new StringBuilder();
            //string isoJson="";
            ///string temp;
            //foreach (var log in args.SavingString.raportService)
            //{
                var temp = JsonConvert.SerializeObject(args.SavingString);
                //isoJson = temp + isoJson;
            //}

            string a = GetString("podaj nazwe pliku\n");
            System.IO.File.WriteAllText(a+".json", temp); ;
            Console.WriteLine(temp);
        }

        public void OnSaved(RaportServive a)
        {

            if (Save != null)
            {
                var savedString = new SavingEventArgs();
                savedString.SavingString = a;
                
                Save(this, savedString);
            }

        } 

        public void AddHomework()
        {
            var courseName = CheckCoursNameInDBisExist(GetString("Podaj nazwę kursu\n"));
            var startDate = GetDate("Podaj date pracy domowej\n");
            var maxScore = GetInt("Podaj maksymalna liczbe punktów za zadanie domowe\n");
            var list = _studentService.GetStudentsInCourse(courseName);
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine("Ile punktów uzyskał " + _studentService.GetStudentNameById(list[i]) + " za zadania domowe");
                var score = GetInt("\n");
                _courseService.AddHomeworks(courseName, score,maxScore, startDate, list[i]); 

            }


        }

        public void AddPresenceDay()
        {
           
            var courseName = CheckCoursNameInDBisExist(GetString("Podaj nazwę kursu\n"));
            var startDate = GetDate("Podaj date obecności\n");
            var list = _studentService.GetStudentsInCourse(courseName);
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine("czy student "+_studentService.GetStudentNameById(list[i])+" był obecny");
                var presence = GetInt("0:Nie 1:Tak\n");
                _courseService.AddPresenDay(list[i],presence,startDate,courseName);
            }
        }
        
        public string CheckCoursNameInDB(string text)
        {
            //var checkCourseName = new CourseService();
            //var checkCourseName = ConfigureContainer().Get<CourseService>();


            bool check = _courseService.CheckIfCourseNameIsInDB(text);
            if (check == false)
            {
                Console.WriteLine("Podana nazwa kursu jest już w bazie");
                return CheckCoursNameInDB(GetString("Podaj nazwę kursu\n")); ;
            }
            else
            {
                return text;
            }

        }

        public string CheckCoursNameInDBisExist(string text)
        {

            bool check = _courseService.CheckIfCourseNameIsInDB(text);
            if (check == true)
            {
                Console.WriteLine("Brak kursu w bazie w bazie");
                return CheckCoursNameInDBisExist(GetString("Podaj nazwę kursu\n")); ;
            }
            else
            {
                return text;
            }

        }

        public void CreateCourse()
        {
            var course = new CourseDto();
            course.CourseName = CheckCourseNameByRegex(CheckCoursNameInDB(GetString("Podaj nazwę kursu\n")));
            
            course.CourseOwner = GetString("Podaj imię prowadzącego\n");
            course.CourseDate = GetDate("Podaj date rozoczęcia kursu\n");
            course.PercentHomework = GetPercent("Podaj procent zadań domowych do zalicznia\n");
            course.PercentAbsence = GetPercent("Podaj procent obecności do zalicznia\n");
            int tempStudentsCount = GetStudentsCount(GetInt("Podaj liczbę studentów\n"));

            _courseService.AddCourse(course);
            for (int i = 0; i < tempStudentsCount; i++)
            {
                var studentPesel = CheckPeselIsInDB("Podaj pesel studenta którego chcesz dodac");
                _studentService.AddStudentByPeselToCourses(studentPesel,course.CourseName);
            }
        }

        

        public void CreateStudentList()
        {
            
            var studentsCount = GetInt("Podaj liczbę studentówj jaką chcesz wprowadzic\n");
            for (int i = 0; i < studentsCount; i++)
            {
                var student = new StudentsDto();
                student.Name = GetString("Podaj imię\n");
                student.Surname = GetString("Podaj nazwisko\n");
                student.DateOfBirth = GetDate("Podaj datę urodzenia\n");
                student.Pesel = GetPesel("Podaj Pesel");
                student.Sex = GetSex("Podaj płeć\n");
                _studentService.addStudent(student);
            }
            

        }

        public int GetStudentsCount(int studentCount)
        {
            int studenCount;


            bool check = _studentService.CheckHowMuchStudentsIs(studentCount);
            if (check == false)
            {
                Console.Write("Studentów jest mniej niż miejsc na kursie\n");
                return GetStudentsCount(GetInt("Podaj liczbę studentów\n"));
            }
            else
            {
                return studentCount;
            }
        }


        public static DateTime GetDate(string text)
        {
            DateTime tempmsg;
            Console.Write(text);

            while (!DateTime.TryParse(Console.ReadLine(), out tempmsg))
            {
                Console.WriteLine("Not an DateTime - try again...");
            }

            return tempmsg;
        }
        public static int GetInt(string message)
        {
            int number;
            Console.Write(message);

            while (!int.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("Not an integer number - try again...");
            }

            return number;
        }
        public int GetSex(string text)
        {
            int tempmsg;
            Console.Write(text);

            while (!int.TryParse(Console.ReadLine(), out tempmsg))
            {
                Console.WriteLine("Not an integer number - try again...");
            }
            if (tempmsg > 0 && tempmsg < 3)
            {
                return tempmsg;
            }
            else
            {
                
                return GetSex("Złe dane podaj ponownie 1:M 2:K\n");
            }
        }

        public static string GetString(string text)
        {
            string tempString;
            Console.Write(text);

            tempString = Console.ReadLine();

            return tempString;

        }

        public int GetPesel(string text)
        {
            int tempPesel;
            Console.WriteLine(text);
           
            while (!int.TryParse(Console.ReadLine(), out tempPesel))
            {
                Console.WriteLine("Not an int number - podaj ponownie pesel");
            }

            //var studentCheckPesel = new StudentService();
            var studentCheckPesel = ConfigureContainer().Get<StudentService>();

            bool check = _studentService.CheckIfStudentExistsInDBByPesel(tempPesel);
            if (check == false)
            {
                Console.WriteLine("Pesel jest juz w bazie");
                return GetPesel("Podaj pesel");
            }
            else
            {
                return tempPesel;
            }
            
        }
        public int GetPercent(string text)
        {
            int tempmsg;
            Console.Write(text);

            while (!int.TryParse(Console.ReadLine(), out tempmsg))
            {
                Console.WriteLine("Not an integer number - try again...");
            }
            if (tempmsg >= 0 && tempmsg <= 100)
            {
                return tempmsg;
            }
            else
            {

                return GetPercent("Złe dane \n");
            }
        }

        public int CheckPeselIsInDB(string text)
        {
            int tempPesel;
            Console.WriteLine(text);

            while (!int.TryParse(Console.ReadLine(), out tempPesel))
            {
                Console.WriteLine("Not an percente number - try again...");
            }

            //var studentCheckPesel = new StudentService();
            //var studentCheckPesel = ConfigureContainer().Get<StudentService>();

            bool check = _studentService.CheckIfStudentExistsInDBByPesel(tempPesel);
            if (check == true)
            {
                Console.WriteLine("Brak peselu w bazie");
                return CheckPeselIsInDB("Podaj pesel");
            }
            else
            {
                return tempPesel;
            }

        }

        private static IKernel ConfigureContainer()
        {
            var kernel = new StandardKernel(new ReportModule());
            return kernel;
        }


    }
}
