﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZadDomTydz2
{
        internal class CommandDispatcher
        {
            public delegate void CommandHandler();
            private readonly Dictionary<int, CommandHandler> _commands = new Dictionary<int, CommandHandler>();

            public bool CheckIfCommandExists(int command)
            {
                return _commands.ContainsKey(command);
            }

            public void AddCommand(int command, CommandHandler handler)
            {
                if (CheckIfCommandExists(command))
                {
                    throw new Exception("Command " + command + " already defined!");
                }

                _commands.Add(command, handler);
            }

            public void ExecuteCommand(int command)
            {
                if (!CheckIfCommandExists(command))
                {
                    throw new Exception("Command " + command + " not defined! Cannot execute!");
                }

                var handler = _commands[command];
                handler();
            }
        }
    }

