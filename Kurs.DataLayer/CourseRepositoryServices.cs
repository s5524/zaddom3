﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookStores.DataLayer.Repositories;
using Kurs.DataLayer.DbContexts;
using Kurs.DataLayer.Models;

namespace Kurs.DataLayer
{
    public class CourseRepositoryServices
    {
        private KursDbContext _dbContext;
        //private DbSet<T> DataSet => _dbContext.Set<T>();



        public IQueryable<Course> GetCoursesWitchHomework()
        {
            var unitOfWork = CourseUnitOfWork.GetInstance();
            var CourseRepository = unitOfWork.GetRepository<Course>();
            return CourseRepository.GetAll().Include(x => x.Homeworks).Include(y => y.Presence);
            
        }

        public IQueryable<Student> GetStudents()
        {
            var unitOfWork = CourseUnitOfWork.GetInstance();
            var StudentRepository = unitOfWork.GetRepository<Student>();
            return StudentRepository.GetAll().Include(x => x.Presences).Include(y => y.Homeworks)
                .Include(z => z.Courses);
        }

        public IQueryable<Presence> GetPresence()
        {
            var unitOfWork = CourseUnitOfWork.GetInstance();

            var PresenceRepository = unitOfWork.GetRepository<Presence>();
            return PresenceRepository.GetAll();

        }

        public IQueryable<Homework> GetHomework()
        {
            var unitOfWork = CourseUnitOfWork.GetInstance();

            var HomeworkRepository = unitOfWork.GetRepository<Homework>();
            return HomeworkRepository.GetAll();


        }

        public bool checkStudentsCount(int count)
        {
            var unitOfWork = CourseUnitOfWork.GetInstance();

            var studentReposietory = unitOfWork.GetRepository<Student>();
            var studentsCount = 0;
            var tempCount = studentReposietory.GetAll().ToList();
            foreach (var log in tempCount)
            {
                studentsCount++;
            }
            if (studentsCount >= count)
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        public void RemoveStudent(Student studentToRemove, Course courses)
        {
            var unitOfWork = CourseUnitOfWork.GetInstance();

            var studentReposietory = unitOfWork.GetRepository<Student>();
            var courseReposietory = unitOfWork.GetRepository<Course>();


            var student = studentReposietory.GetAll().FirstOrDefault(p => p.Id == studentToRemove.Id);
            var cours = courseReposietory.GetAll().Include(x => x.Students).FirstOrDefault(s => s.Id == courses.Id);

            courseReposietory.Attath(cours);

            cours.Students.Remove(student);
        }
        public void AddStudentByPeselToCourse(int pesel, string courseName)
        {
            var unitOfWork = CourseUnitOfWork.GetInstance();

            var studentReposietory = unitOfWork.GetRepository<Student>();
            var courseReposietory = unitOfWork.GetRepository<Course>();


            var list = courseReposietory.GetAll().Include(x => x.Students);
                var courses = list.First(x => x.CourseName == courseName);

                var student = studentReposietory.GetAll().First(x => x.Pesel == pesel);
            //studentReposietory.Attath(student);
            courses.Students.Add(student);
            courseReposietory.Attath(courses);
            
            //courseReposietory.Add(courses);

            //_dbContext.SaveChanges();
        }

        public List<int> GetAllStudentsInCourse(string courseName)
        {
            List<int> studentIds = new List<int>();
            var unitOfWork = CourseUnitOfWork.GetInstance();

            var studentReposietory = unitOfWork.GetRepository<Student>();
            var courseReposietory = unitOfWork.GetRepository<Course>();

            var students = courseReposietory.GetAll().Include(x => x.Students);
            var course = students.First(x => x.CourseName == courseName);
            foreach (var log in course.Students)
            {
                //Console.WriteLine(log.Id);
                studentIds.Add(log.Id);

            }

            return studentIds;
        }

        public void AddPresenceDay(Presence presences, string name, int id)
        {
            var unitOfWork = CourseUnitOfWork.GetInstance();

            var courseReposietory = unitOfWork.GetRepository<Course>();

            var course = courseReposietory.GetAll().Include(x => x.Students);
            var students = course.First(y => y.Id == id);
        }
    }
}


