﻿using System;
using System.Collections.Generic;
using Kurs.DataLayer.DbContexts;
using Kurs.DataLayer.Models;
using Kurs.DataLayer.Repositories.Interfaces;

namespace BookStores.DataLayer.Repositories
{
    public class CourseUnitOfWork
    {
        private KursDbContext _dbContext;
        private Dictionary<Type, object> _repositories;

        public static CourseUnitOfWork _instance;
        //public GenericRepository<Book> BooksRepository { get; private set; }

        public static CourseUnitOfWork GetInstance()
        {
            if (_instance == null)
            {
                _instance = new CourseUnitOfWork();
                //_instance.RegisterRepositories();

            }
            return _instance;
        }

        private CourseUnitOfWork()
        {
            _dbContext = new KursDbContext();
            RegisterRepositories();
        }

        private void RegisterRepositories()
        {
            _repositories = new Dictionary<Type, object>();

            Register(typeof(Student), new GenericRepository<Student>(_dbContext));
            Register(typeof(Presence), new GenericRepository<Presence>(_dbContext));
            Register(typeof(Homework), new GenericRepository<Homework>(_dbContext));

            Register(typeof(Course), new GenericRepository<Course>(_dbContext));
        }

        private GenericRepository<Course> _genericRepositoryCourse;
        private GenericRepository<Student> _genericRepositoryStudent;
        private GenericRepository<Presence> _genericRepositoryP;
        private GenericRepository<Homework> _genericRepositoryH;



        private void Register(Type type, object repository)
        {
            if (_repositories.ContainsKey(type))
            {
                throw new Exception("There is no reposioetories for type " + type.Name);
            }

            _repositories.Add(type, repository);
        }

        public GenericRepository<T> GetRepository<T>() where T : class, IEntity
        {
            return _repositories[typeof(T)] as GenericRepository<T>;
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }
    }
}
