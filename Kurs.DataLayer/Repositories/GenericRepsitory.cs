﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Kurs.DataLayer.DbContexts;
using Kurs.DataLayer.Repositories.Interfaces;
//using Kurs.DataLayer.Repositories.Interfaces;

namespace BookStores.DataLayer.Repositories
{
    public class GenericRepository<T> where T : class, IEntity
    {
        private KursDbContext _dbContext;
        private DbSet<T> DataSet => _dbContext.Set<T>();

        public GenericRepository(KursDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public bool Add(T entity)
        {
            if (true)
            {
                DataSet.Add(entity);
                return true;
            }

        }
        public void Attath(T entity)
        {
            if (true)
            {
               DataSet.Attach(entity);
            }

        }

       
        public T Get(int Id)
        {
            return GetAll().Single(i => i.Id == Id);
        }

        public IQueryable<T> GetAll()
        {
            return DataSet;
        }

        public void Update(T entity)
        {
            if (!DataSet.Local.Contains(entity))
            {
                DataSet.AddOrUpdate(entity);
            }
        }

        public void Delete(int Id)
        {
            DataSet.Remove(Get(Id));
        }


    }
}

