﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kurs.DataLayer.Models;

namespace Kurs.DataLayer.DbContexts
{
    public class KursDbContext : DbContext
    {
        

        public KursDbContext() : base(GetConnectionString())
        { }

        public DbSet<Student> StudentDbSet { get; set; }
        public DbSet<Course> CourseDbSet { get; set; }
        public DbSet<Presence> PresenceDbSet { get; set; }
        public DbSet<Homework> HomeworkDbSet { get; set; }


        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["KursDatabase"].ConnectionString;
        }
    }

}
