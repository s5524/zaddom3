﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kurs.DataLayer.Repositories.Interfaces;

namespace Kurs.DataLayer.Models
{
    public class Student : IEntity
    {
        
        public int Id { get; set; }
        public int Pesel { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Sex { get; set; }
        public List<Course> Courses { get; set; }
        public List<Presence> Presences { get; set; }
        public List<Homework> Homeworks { get; set; }
        
        public override bool Equals(object obj)
        {
            if (obj == null || obj as Student == null)//strawdza w drzewie dziedziczenia czy moze zrobic ksiazke'sprawdzanie typu'
            {
                return false;
            }
            var objAsStudent = obj as Student;
            var areEqyal = true;
            areEqyal = areEqyal && ((Student)obj).Id == Id;
            areEqyal = areEqyal && objAsStudent.Pesel == Pesel;
            areEqyal = areEqyal && objAsStudent.Name == Name;
            areEqyal = areEqyal && objAsStudent.Surname == Surname;
            areEqyal = areEqyal && objAsStudent.DateOfBirth == DateOfBirth;
            areEqyal = areEqyal && objAsStudent.Sex == Sex;
            return areEqyal;
        }
    }
}
