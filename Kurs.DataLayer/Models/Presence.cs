﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kurs.DataLayer.Repositories.Interfaces;

namespace Kurs.DataLayer.Models
{
    public class Presence : IEntity
    {
        public int Id { get; set; }
        public DateTime PresencDate { get; set; }
        public int IsPresence { get; set; }
        public Course Course { get; set; }
        public Student Student { get; set; }
        
    }
}
